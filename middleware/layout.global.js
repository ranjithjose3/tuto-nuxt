export default defineNuxtRouteMiddleware((to) => {
    if (to.fullPath.startsWith('/users')) {
        setPageLayout('admin')
      } else {
        setPageLayout('default')
      }
  })