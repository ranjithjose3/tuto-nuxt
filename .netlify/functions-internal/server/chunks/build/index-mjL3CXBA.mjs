import { _ as _export_sfc, b as __nuxt_component_0$1 } from './server.mjs';
import { _ as __nuxt_component_1 } from './nuxt-link-BFCqUiHC.mjs';
import { withCtx, createTextVNode, toDisplayString, createVNode, openBlock, createBlock, Fragment, renderList, useSSRContext } from 'vue';
import { ssrRenderAttrs, ssrRenderComponent, ssrInterpolate, ssrRenderList } from 'vue/server-renderer';
import '../runtime.mjs';
import 'node:http';
import 'node:https';
import 'fs';
import 'path';
import '../routes/renderer.mjs';
import 'vue-bundle-renderer/runtime';
import 'devalue';
import '@unhead/ssr';
import 'unhead';
import '@unhead/shared';
import 'vue-router';

const _sfc_main = {};
function _sfc_ssrRender(_ctx, _push, _parent, _attrs) {
  const _component_NuxtLayout = __nuxt_component_0$1;
  const _component_NuxtLink = __nuxt_component_1;
  _push(`<div${ssrRenderAttrs(_attrs)}>`);
  _push(ssrRenderComponent(_component_NuxtLayout, { name: "custom" }, {
    default: withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`<h1${_scopeId}>Category Page: ${ssrInterpolate(_ctx.$route.params.category)}</h1><ul${_scopeId}><!--[-->`);
        ssrRenderList(10, (product) => {
          _push2(`<li${_scopeId}>`);
          _push2(ssrRenderComponent(_component_NuxtLink, {
            to: `/categories/${_ctx.$route.params.category}/product-${product}`
          }, {
            default: withCtx((_2, _push3, _parent3, _scopeId2) => {
              if (_push3) {
                _push3(` Product ${ssrInterpolate(product)}`);
              } else {
                return [
                  createTextVNode(" Product " + toDisplayString(product), 1)
                ];
              }
            }),
            _: 2
          }, _parent2, _scopeId));
          _push2(`</li>`);
        });
        _push2(`<!--]--></ul>`);
      } else {
        return [
          createVNode("h1", null, "Category Page: " + toDisplayString(_ctx.$route.params.category), 1),
          createVNode("ul", null, [
            (openBlock(), createBlock(Fragment, null, renderList(10, (product) => {
              return createVNode("li", { key: product }, [
                createVNode(_component_NuxtLink, {
                  to: `/categories/${_ctx.$route.params.category}/product-${product}`
                }, {
                  default: withCtx(() => [
                    createTextVNode(" Product " + toDisplayString(product), 1)
                  ]),
                  _: 2
                }, 1032, ["to"])
              ]);
            }), 64))
          ])
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(`</div>`);
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("pages/categories/[category]/index.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const index = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);

export { index as default };
//# sourceMappingURL=index-mjL3CXBA.mjs.map
