import { c as setPageLayout, s as script } from './server.mjs';
import { useSSRContext } from 'vue';
import { ssrRenderAttrs, ssrRenderComponent } from 'vue/server-renderer';
import '../runtime.mjs';
import 'node:http';
import 'node:https';
import 'fs';
import 'path';
import '../routes/renderer.mjs';
import 'vue-bundle-renderer/runtime';
import 'devalue';
import '@unhead/ssr';
import 'unhead';
import '@unhead/shared';
import 'vue-router';

const _sfc_main = {
  __name: "index",
  __ssrInlineRender: true,
  setup(__props) {
    const enableCustomLayout = () => {
      setPageLayout("custom");
    };
    return (_ctx, _push, _parent, _attrs) => {
      const _component_PrimeButton = script;
      _push(`<div${ssrRenderAttrs(_attrs)}><h1>Hello world</h1>`);
      _push(ssrRenderComponent(_component_PrimeButton, {
        onClick: enableCustomLayout,
        label: "Set Custom Layout"
      }, null, _parent));
      _push(`</div>`);
    };
  }
};
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("pages/index.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};

export { _sfc_main as default };
//# sourceMappingURL=index-CbThbH4T.mjs.map
