import { l as defineNuxtRouteMiddleware } from './server.mjs';
import 'vue';
import '../runtime.mjs';
import 'node:http';
import 'node:https';
import 'fs';
import 'path';
import '../routes/renderer.mjs';
import 'vue-bundle-renderer/runtime';
import 'devalue';
import 'vue/server-renderer';
import '@unhead/ssr';
import 'unhead';
import '@unhead/shared';
import 'vue-router';

const middlewareExample = defineNuxtRouteMiddleware((to, from) => {
  console.log("This is about us page file middleware");
});

export { middlewareExample as default };
//# sourceMappingURL=middleware-example-Bo52SFKu.mjs.map
