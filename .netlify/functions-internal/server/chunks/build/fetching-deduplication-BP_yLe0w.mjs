import { s as script } from './server.mjs';
import { ref, withAsyncContext, unref, useSSRContext } from 'vue';
import { u as useFetch } from './fetch-DM1gr9br.mjs';
import { ssrRenderAttrs, ssrInterpolate, ssrRenderComponent } from 'vue/server-renderer';
import '../runtime.mjs';
import 'node:http';
import 'node:https';
import 'fs';
import 'path';
import '../routes/renderer.mjs';
import 'vue-bundle-renderer/runtime';
import 'devalue';
import '@unhead/ssr';
import 'unhead';
import '@unhead/shared';
import 'vue-router';

const intervalError = "[nuxt] `setInterval` should not be used on the server. Consider wrapping it with an `onNuxtReady`, `onBeforeMount` or `onMounted` lifecycle hook, or ensure you only call it in the browser by checking `false`.";
const setInterval = () => {
  console.error(intervalError);
};
const _sfc_main = {
  __name: "fetching-deduplication",
  __ssrInlineRender: true,
  async setup(__props) {
    let __temp, __restore;
    ref(1);
    const { data: product, refresh } = ([__temp, __restore] = withAsyncContext(() => useFetch("https://fakestoreapi.com/products/1", "$7kCUBCvTT8")), __temp = await __temp, __restore(), __temp);
    console.log(product.value);
    const fetchData = () => {
      const interval = setInterval();
      setTimeout(() => {
        clearInterval(interval);
      }, 2e3);
    };
    return (_ctx, _push, _parent, _attrs) => {
      const _component_PrimeButton = script;
      _push(`<div${ssrRenderAttrs(_attrs)}>${ssrInterpolate(unref(product))} <br><br>`);
      _push(ssrRenderComponent(_component_PrimeButton, {
        onClick: fetchData,
        label: "Disply a new product"
      }, null, _parent));
      _push(`</div>`);
    };
  }
};
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("pages/fetching/fetching-deduplication.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};

export { _sfc_main as default };
//# sourceMappingURL=fetching-deduplication-BP_yLe0w.mjs.map
