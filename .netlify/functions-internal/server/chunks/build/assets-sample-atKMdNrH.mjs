import { p as publicAssetsURL, a as buildAssetsURL } from '../routes/renderer.mjs';
import { u as useHead, a as useSeoMeta, H as Head, T as Title } from './components-B_clKsUA.mjs';
import { withCtx, createTextVNode, createVNode, useSSRContext } from 'vue';
import { ssrRenderAttrs, ssrRenderAttr, ssrRenderComponent } from 'vue/server-renderer';
import { _ as _export_sfc } from './server.mjs';
import 'vue-bundle-renderer/runtime';
import '../runtime.mjs';
import 'node:http';
import 'node:https';
import 'fs';
import 'path';
import 'devalue';
import '@unhead/ssr';
import 'unhead';
import '@unhead/shared';
import 'vue-router';

const _imports_0 = publicAssetsURL("/images/nuxt.png");
const _imports_1 = "" + buildAssetsURL("nuxt-asset.Do-rDTHT.png");
const _sfc_main = {
  __name: "assets-sample",
  __ssrInlineRender: true,
  setup(__props) {
    useHead({
      title: "Asset and CSS tuto Page"
    });
    useSeoMeta({
      title: "Asset and CSS tuto Page for SEO"
    });
    return (_ctx, _push, _parent, _attrs) => {
      const _component_Head = Head;
      const _component_Title = Title;
      _push(`<div${ssrRenderAttrs(_attrs)} data-v-ad6ebf9c><h2 class="animate__animated animate__bounce" data-v-ad6ebf9c>Asset and css Sample</h2><h4 data-v-ad6ebf9c>Image from Public</h4><img width="20%"${ssrRenderAttr("src", _imports_0)} alt="Image from public" data-v-ad6ebf9c><h4 data-v-ad6ebf9c>Image from Assets</h4><img width="20%"${ssrRenderAttr("src", _imports_1)} alt="Image from public" data-v-ad6ebf9c><p data-v-ad6ebf9c> Sample Lorem ipsum dolor sit amet consectetur adipisicing elit. Ducimus necessitatibus repellat sint nobis veritatis accusantium excepturi? Sit debitis soluta nisi harum error voluptatibus explicabo ipsum excepturi delectus, in, laboriosam ea? </p>`);
      _push(ssrRenderComponent(_component_Head, null, {
        default: withCtx((_, _push2, _parent2, _scopeId) => {
          if (_push2) {
            _push2(ssrRenderComponent(_component_Title, null, {
              default: withCtx((_2, _push3, _parent3, _scopeId2) => {
                if (_push3) {
                  _push3(` Asset page titile from component `);
                } else {
                  return [
                    createTextVNode(" Asset page titile from component ")
                  ];
                }
              }),
              _: 1
            }, _parent2, _scopeId));
          } else {
            return [
              createVNode(_component_Title, null, {
                default: withCtx(() => [
                  createTextVNode(" Asset page titile from component ")
                ]),
                _: 1
              })
            ];
          }
        }),
        _: 1
      }, _parent));
      _push(`</div>`);
    };
  }
};
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("pages/assets-sample.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const assetsSample = /* @__PURE__ */ _export_sfc(_sfc_main, [["__scopeId", "data-v-ad6ebf9c"]]);

export { assetsSample as default };
//# sourceMappingURL=assets-sample-atKMdNrH.mjs.map
