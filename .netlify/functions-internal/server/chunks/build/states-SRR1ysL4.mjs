import { u as useState } from './state-PCZ5p3X2.mjs';

const useCounter = () => {
  return useState("compsCounterState", () => 1);
};

export { useCounter as u };
//# sourceMappingURL=states-SRR1ysL4.mjs.map
