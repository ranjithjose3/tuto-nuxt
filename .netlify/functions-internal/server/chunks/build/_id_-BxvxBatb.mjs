import { u as useRoute, d as createError } from './server.mjs';
import { u as useFetch } from './fetch-DM1gr9br.mjs';
import { withAsyncContext, unref, useSSRContext } from 'vue';
import { ssrRenderAttrs, ssrInterpolate } from 'vue/server-renderer';
import '../runtime.mjs';
import 'node:http';
import 'node:https';
import 'fs';
import 'path';
import '../routes/renderer.mjs';
import 'vue-bundle-renderer/runtime';
import 'devalue';
import '@unhead/ssr';
import 'unhead';
import '@unhead/shared';
import 'vue-router';

const _sfc_main = {
  __name: "[id]",
  __ssrInlineRender: true,
  async setup(__props) {
    let __temp, __restore;
    const route = useRoute();
    const id = Number(route.params.id);
    const { data: product, error } = ([__temp, __restore] = withAsyncContext(() => useFetch(`https://fakestoreapi.com/products/${id}`, {}, "$wpXHD4Yxv9")), __temp = await __temp, __restore(), __temp);
    if (!product.value) {
      throw createError({
        statusCode: 404,
        message: "Page Not Found! custom"
      });
    }
    console.log(error);
    return (_ctx, _push, _parent, _attrs) => {
      _push(`<div${ssrRenderAttrs(_attrs)}>`);
      if (unref(error)) {
        _push(`<p>${ssrInterpolate(unref(error))}</p>`);
      } else {
        _push(`<p>${ssrInterpolate(unref(product))}</p>`);
      }
      _push(`</div>`);
    };
  }
};
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("pages/product/[id].vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};

export { _sfc_main as default };
//# sourceMappingURL=_id_-BxvxBatb.mjs.map
