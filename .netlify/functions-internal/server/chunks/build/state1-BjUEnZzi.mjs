import { s as script } from './server.mjs';
import { _ as __nuxt_component_1 } from './nuxt-link-BFCqUiHC.mjs';
import { u as useState } from './state-PCZ5p3X2.mjs';
import { u as useCounter } from './states-SRR1ysL4.mjs';
import { unref, withCtx, createTextVNode, useSSRContext } from 'vue';
import { ssrRenderAttrs, ssrInterpolate, ssrRenderComponent } from 'vue/server-renderer';
import '../runtime.mjs';
import 'node:http';
import 'node:https';
import 'fs';
import 'path';
import '../routes/renderer.mjs';
import 'vue-bundle-renderer/runtime';
import 'devalue';
import '@unhead/ssr';
import 'unhead';
import '@unhead/shared';
import 'vue-router';

const _sfc_main = {
  __name: "state1",
  __ssrInlineRender: true,
  setup(__props) {
    const counter = useState(() => 1, "$uhJptwUUCP");
    const counterState = useState("counterState", () => 1);
    const counterCompos = useCounter();
    return (_ctx, _push, _parent, _attrs) => {
      const _component_PrimeButton = script;
      const _component_NuxtLink = __nuxt_component_1;
      _push(`<div${ssrRenderAttrs(_attrs)}><h1>Counter : ${ssrInterpolate(unref(counter))}</h1><h1> Counter Shared State : ${ssrInterpolate(unref(counterState))}</h1><h1> Counter Compos State : ${ssrInterpolate(unref(counterCompos))}</h1><h1> Counter Shallow State : ${ssrInterpolate(unref(counterCompos))}</h1>`);
      _push(ssrRenderComponent(_component_PrimeButton, {
        onClick: ($event) => counter.value++,
        label: "Increment"
      }, null, _parent));
      _push(ssrRenderComponent(_component_NuxtLink, { to: "/states/state2" }, {
        default: withCtx((_, _push2, _parent2, _scopeId) => {
          if (_push2) {
            _push2(`State 2`);
          } else {
            return [
              createTextVNode("State 2")
            ];
          }
        }),
        _: 1
      }, _parent));
      _push(ssrRenderComponent(_component_NuxtLink, { to: "/states/shared-state" }, {
        default: withCtx((_, _push2, _parent2, _scopeId) => {
          if (_push2) {
            _push2(`Shared State `);
          } else {
            return [
              createTextVNode("Shared State ")
            ];
          }
        }),
        _: 1
      }, _parent));
      _push(ssrRenderComponent(_component_NuxtLink, { to: "/states/compos-state" }, {
        default: withCtx((_, _push2, _parent2, _scopeId) => {
          if (_push2) {
            _push2(`Compos State `);
          } else {
            return [
              createTextVNode("Compos State ")
            ];
          }
        }),
        _: 1
      }, _parent));
      _push(ssrRenderComponent(_component_NuxtLink, { to: "/states/shallow" }, {
        default: withCtx((_, _push2, _parent2, _scopeId) => {
          if (_push2) {
            _push2(`Shallow State `);
          } else {
            return [
              createTextVNode("Shallow State ")
            ];
          }
        }),
        _: 1
      }, _parent));
      _push(`</div>`);
    };
  }
};
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("pages/states/state1.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};

export { _sfc_main as default };
//# sourceMappingURL=state1-BjUEnZzi.mjs.map
