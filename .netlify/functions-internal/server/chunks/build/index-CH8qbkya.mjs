import { defineAsyncComponent, ref, unref, useSSRContext } from 'vue';
import { s as script } from './server.mjs';
import { ssrRenderAttrs, ssrRenderComponent } from 'vue/server-renderer';
import '../runtime.mjs';
import 'node:http';
import 'node:https';
import 'fs';
import 'path';
import '../routes/renderer.mjs';
import 'vue-bundle-renderer/runtime';
import 'devalue';
import '@unhead/ssr';
import 'unhead';
import '@unhead/shared';
import 'vue-router';

const __nuxt_component_0_lazy = defineAsyncComponent(() => import('./Welcome-99ktpQn2.mjs').then((c) => c.default || c));
const _sfc_main = {
  __name: "index",
  __ssrInlineRender: true,
  setup(__props) {
    const show = ref(false);
    return (_ctx, _push, _parent, _attrs) => {
      const _component_LazyWelcome = __nuxt_component_0_lazy;
      const _component_PrimeButton = script;
      _push(`<div${ssrRenderAttrs(_attrs)}><h1>Lazy loading</h1>`);
      if (unref(show)) {
        _push(ssrRenderComponent(_component_LazyWelcome, null, null, _parent));
      } else {
        _push(`<!---->`);
      }
      _push(ssrRenderComponent(_component_PrimeButton, {
        onClick: ($event) => show.value = !unref(show),
        label: "Show Welcome"
      }, null, _parent));
      _push(`</div>`);
    };
  }
};
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("pages/lazy-loading/index.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};

export { _sfc_main as default };
//# sourceMappingURL=index-CH8qbkya.mjs.map
