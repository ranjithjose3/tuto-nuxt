const interopDefault = r => r.default || r || [];
const styles = {
  "node_modules/nuxt/dist/app/entry.js": () => import('./entry-styles.C7zMDc8z.mjs').then(interopDefault),
  "pages/assets-sample.vue": () => import('./assets-sample-styles.DbCmvrLF.mjs').then(interopDefault),
  "pages/assets-sample.vue?vue&type=style&index=0&scoped=ad6ebf9c&lang.css": () => import('./assets-sample-styles.Cgs8j3mU.mjs').then(interopDefault)
};

export { styles as default };
//# sourceMappingURL=styles.mjs.map
