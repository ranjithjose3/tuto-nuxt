const client_manifest = {
  "_BOyqhDOU.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "BOyqhDOU.js",
    "name": "index.esm",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "_BWk1zTJo.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "BWk1zTJo.js",
    "name": "index.esm",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "_BhPlg7EZ.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "BhPlg7EZ.js",
    "name": "index.esm",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "_BpvOsy_P.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "BpvOsy_P.js",
    "name": "index.esm",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "_BwBznSD0.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "BwBznSD0.js",
    "name": "components",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "_CWV7Ebdt.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "CWV7Ebdt.js",
    "name": "index.esm",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "_CYl7PQbN.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "CYl7PQbN.js",
    "name": "index.esm",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "_C_uFE9Zw.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "C_uFE9Zw.js",
    "name": "index.esm",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "_CygsLJbz.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "CygsLJbz.js",
    "name": "fetch",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "_D0W5RRF9.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "D0W5RRF9.js",
    "name": "overlayeventbus.esm",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "_D24Q6d7X.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "D24Q6d7X.js",
    "name": "index.esm",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "_D6mLI-j_.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "D6mLI-j_.js",
    "name": "state",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "_D9Qh6dT5.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "D9Qh6dT5.js",
    "name": "index.esm",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "_DNYGth56.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "DNYGth56.js",
    "name": "index.esm",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "_DSTw9jQC.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "DSTw9jQC.js",
    "name": "index.esm",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "_DXGnkx1y.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "DXGnkx1y.js",
    "name": "index.esm",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "_DXiRM4vO.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "DXiRM4vO.js",
    "name": "portal.esm",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "_DfgDGet4.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "DfgDGet4.js",
    "name": "index.esm",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "_DiQfV1Th.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "DiQfV1Th.js",
    "name": "index.esm",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "_Dsisqutq.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "Dsisqutq.js",
    "name": "index.esm",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "_HJSzoE6Z.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "HJSzoE6Z.js",
    "name": "index.esm",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "_HOLQHh74.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "HOLQHh74.js",
    "name": "states",
    "imports": [
      "_D6mLI-j_.js"
    ]
  },
  "_mvKC_REM.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "mvKC_REM.js",
    "name": "index.esm",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "_p_ZPdm--.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "p_ZPdm--.js",
    "name": "index.esm",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "_pb5u_aX1.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "pb5u_aX1.js",
    "name": "index.esm",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "_qMGSVOoo.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "qMGSVOoo.js",
    "name": "nuxt-link",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "assets/images/nuxt-asset.png": {
    "resourceType": "image",
    "prefetch": true,
    "mimeType": "image/png",
    "file": "nuxt-asset.Do-rDTHT.png",
    "src": "assets/images/nuxt-asset.png"
  },
  "components/Welcome.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "qkzjZgt2.js",
    "name": "Welcome",
    "src": "components/Welcome.vue",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "layouts/admin.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "Bx6f_62q.js",
    "name": "admin",
    "src": "layouts/admin.vue",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "layouts/custom.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "DFiPgWv6.js",
    "name": "custom",
    "src": "layouts/custom.vue",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "layouts/default.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "Co9OkHAg.js",
    "name": "default",
    "src": "layouts/default.vue",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "middleware/middleware-example.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "DEPh1EPX.js",
    "name": "middleware-example",
    "src": "middleware/middleware-example.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/chart.js/auto/auto.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "CAdRPfCH.js",
    "name": "auto",
    "src": "node_modules/chart.js/auto/auto.js",
    "isDynamicEntry": true
  },
  "node_modules/nuxt/dist/app/entry.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "D4O5gdP8.js",
    "name": "entry",
    "src": "node_modules/nuxt/dist/app/entry.js",
    "isEntry": true,
    "dynamicImports": [
      "middleware/middleware-example.js",
      "node_modules/primevue/autocomplete/autocomplete.esm.js",
      "node_modules/primevue/calendar/calendar.esm.js",
      "node_modules/primevue/cascadeselect/cascadeselect.esm.js",
      "node_modules/primevue/checkbox/checkbox.esm.js",
      "node_modules/primevue/chips/chips.esm.js",
      "node_modules/primevue/colorpicker/colorpicker.esm.js",
      "node_modules/primevue/dropdown/dropdown.esm.js",
      "node_modules/primevue/editor/editor.esm.js",
      "node_modules/primevue/floatlabel/floatlabel.esm.js",
      "node_modules/primevue/iconfield/iconfield.esm.js",
      "node_modules/primevue/inputgroup/inputgroup.esm.js",
      "node_modules/primevue/inputgroupaddon/inputgroupaddon.esm.js",
      "node_modules/primevue/inputicon/inputicon.esm.js",
      "node_modules/primevue/inputmask/inputmask.esm.js",
      "node_modules/primevue/inputnumber/inputnumber.esm.js",
      "node_modules/primevue/inputotp/inputotp.esm.js",
      "node_modules/primevue/inputswitch/inputswitch.esm.js",
      "node_modules/primevue/inputtext/inputtext.esm.js",
      "node_modules/primevue/knob/knob.esm.js",
      "node_modules/primevue/listbox/listbox.esm.js",
      "node_modules/primevue/multiselect/multiselect.esm.js",
      "node_modules/primevue/password/password.esm.js",
      "node_modules/primevue/radiobutton/radiobutton.esm.js",
      "node_modules/primevue/rating/rating.esm.js",
      "node_modules/primevue/selectbutton/selectbutton.esm.js",
      "node_modules/primevue/slider/slider.esm.js",
      "node_modules/primevue/textarea/textarea.esm.js",
      "node_modules/primevue/togglebutton/togglebutton.esm.js",
      "node_modules/primevue/treeselect/treeselect.esm.js",
      "node_modules/primevue/tristatecheckbox/tristatecheckbox.esm.js",
      "node_modules/primevue/buttongroup/buttongroup.esm.js",
      "node_modules/primevue/speeddial/speeddial.esm.js",
      "node_modules/primevue/splitbutton/splitbutton.esm.js",
      "node_modules/primevue/column/column.esm.js",
      "node_modules/primevue/row/row.esm.js",
      "node_modules/primevue/columngroup/columngroup.esm.js",
      "node_modules/primevue/datatable/datatable.esm.js",
      "node_modules/primevue/dataview/dataview.esm.js",
      "node_modules/primevue/dataviewlayoutoptions/dataviewlayoutoptions.esm.js",
      "node_modules/primevue/orderlist/orderlist.esm.js",
      "node_modules/primevue/organizationchart/organizationchart.esm.js",
      "node_modules/primevue/paginator/paginator.esm.js",
      "node_modules/primevue/picklist/picklist.esm.js",
      "node_modules/primevue/tree/tree.esm.js",
      "node_modules/primevue/treetable/treetable.esm.js",
      "node_modules/primevue/timeline/timeline.esm.js",
      "node_modules/primevue/virtualscroller/virtualscroller.esm.js",
      "node_modules/primevue/accordion/accordion.esm.js",
      "node_modules/primevue/accordiontab/accordiontab.esm.js",
      "node_modules/primevue/card/card.esm.js",
      "node_modules/primevue/deferredcontent/deferredcontent.esm.js",
      "node_modules/primevue/divider/divider.esm.js",
      "node_modules/primevue/fieldset/fieldset.esm.js",
      "node_modules/primevue/panel/panel.esm.js",
      "node_modules/primevue/scrollpanel/scrollpanel.esm.js",
      "node_modules/primevue/splitter/splitter.esm.js",
      "node_modules/primevue/splitterpanel/splitterpanel.esm.js",
      "node_modules/primevue/stepper/stepper.esm.js",
      "node_modules/primevue/stepperpanel/stepperpanel.esm.js",
      "node_modules/primevue/tabview/tabview.esm.js",
      "node_modules/primevue/tabpanel/tabpanel.esm.js",
      "node_modules/primevue/toolbar/toolbar.esm.js",
      "node_modules/primevue/confirmdialog/confirmdialog.esm.js",
      "node_modules/primevue/confirmpopup/confirmpopup.esm.js",
      "node_modules/primevue/dialog/dialog.esm.js",
      "node_modules/primevue/dynamicdialog/dynamicdialog.esm.js",
      "node_modules/primevue/overlaypanel/overlaypanel.esm.js",
      "node_modules/primevue/sidebar/sidebar.esm.js",
      "node_modules/primevue/fileupload/fileupload.esm.js",
      "node_modules/primevue/breadcrumb/breadcrumb.esm.js",
      "node_modules/primevue/contextmenu/contextmenu.esm.js",
      "node_modules/primevue/dock/dock.esm.js",
      "node_modules/primevue/menu/menu.esm.js",
      "node_modules/primevue/menubar/menubar.esm.js",
      "node_modules/primevue/megamenu/megamenu.esm.js",
      "node_modules/primevue/panelmenu/panelmenu.esm.js",
      "node_modules/primevue/steps/steps.esm.js",
      "node_modules/primevue/tabmenu/tabmenu.esm.js",
      "node_modules/primevue/tieredmenu/tieredmenu.esm.js",
      "node_modules/primevue/chart/chart.esm.js",
      "node_modules/primevue/message/message.esm.js",
      "node_modules/primevue/inlinemessage/inlinemessage.esm.js",
      "node_modules/primevue/toast/toast.esm.js",
      "node_modules/primevue/image/image.esm.js",
      "node_modules/primevue/avatar/avatar.esm.js",
      "node_modules/primevue/avatargroup/avatargroup.esm.js",
      "node_modules/primevue/blockui/blockui.esm.js",
      "node_modules/primevue/chip/chip.esm.js",
      "node_modules/primevue/inplace/inplace.esm.js",
      "node_modules/primevue/metergroup/metergroup.esm.js",
      "node_modules/primevue/scrolltop/scrolltop.esm.js",
      "node_modules/primevue/skeleton/skeleton.esm.js",
      "node_modules/primevue/progressbar/progressbar.esm.js",
      "node_modules/primevue/progressspinner/progressspinner.esm.js",
      "node_modules/primevue/tag/tag.esm.js",
      "node_modules/primevue/terminal/terminal.esm.js",
      "layouts/admin.vue",
      "layouts/custom.vue",
      "layouts/default.vue"
    ],
    "_globalCSS": true
  },
  "node_modules/primevue/accordion/accordion.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "Bzn7wL49.js",
    "name": "accordion.esm",
    "src": "node_modules/primevue/accordion/accordion.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "_DSTw9jQC.js",
      "_p_ZPdm--.js",
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/accordiontab/accordiontab.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "BaJiVsC1.js",
    "name": "accordiontab.esm",
    "src": "node_modules/primevue/accordiontab/accordiontab.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/autocomplete/autocomplete.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "BQj20nLg.js",
    "name": "autocomplete.esm",
    "src": "node_modules/primevue/autocomplete/autocomplete.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js",
      "_DSTw9jQC.js",
      "_CWV7Ebdt.js",
      "_D0W5RRF9.js",
      "_DXiRM4vO.js",
      "node_modules/primevue/virtualscroller/virtualscroller.esm.js"
    ]
  },
  "node_modules/primevue/avatar/avatar.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "gbH7Mr5A.js",
    "name": "avatar.esm",
    "src": "node_modules/primevue/avatar/avatar.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/avatargroup/avatargroup.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "DcNXTgSZ.js",
    "name": "avatargroup.esm",
    "src": "node_modules/primevue/avatargroup/avatargroup.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/blockui/blockui.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "C2ltku2Q.js",
    "name": "blockui.esm",
    "src": "node_modules/primevue/blockui/blockui.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/breadcrumb/breadcrumb.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "Dxc9ISnq.js",
    "name": "breadcrumb.esm",
    "src": "node_modules/primevue/breadcrumb/breadcrumb.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "_p_ZPdm--.js",
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/buttongroup/buttongroup.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "CuoY6uA_.js",
    "name": "buttongroup.esm",
    "src": "node_modules/primevue/buttongroup/buttongroup.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/calendar/calendar.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "DNYaLPtf.js",
    "name": "calendar.esm",
    "src": "node_modules/primevue/calendar/calendar.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js",
      "_DSTw9jQC.js",
      "_pb5u_aX1.js",
      "_p_ZPdm--.js",
      "_BpvOsy_P.js",
      "_D0W5RRF9.js",
      "_DXiRM4vO.js"
    ]
  },
  "node_modules/primevue/card/card.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "CIhuTBJx.js",
    "name": "card.esm",
    "src": "node_modules/primevue/card/card.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/cascadeselect/cascadeselect.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "BQcvEkvr.js",
    "name": "cascadeselect.esm",
    "src": "node_modules/primevue/cascadeselect/cascadeselect.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "_CYl7PQbN.js",
      "_DSTw9jQC.js",
      "node_modules/nuxt/dist/app/entry.js",
      "_D0W5RRF9.js",
      "_DXiRM4vO.js"
    ]
  },
  "node_modules/primevue/chart/chart.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "pXZcxF9C.js",
    "name": "chart.esm",
    "src": "node_modules/primevue/chart/chart.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ],
    "dynamicImports": [
      "node_modules/chart.js/auto/auto.js"
    ]
  },
  "node_modules/primevue/checkbox/checkbox.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "QsQnBdgE.js",
    "name": "checkbox.esm",
    "src": "node_modules/primevue/checkbox/checkbox.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "_HJSzoE6Z.js",
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/chip/chip.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "DGRR2EiA.js",
    "name": "chip.esm",
    "src": "node_modules/primevue/chip/chip.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "_CWV7Ebdt.js",
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/chips/chips.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "BCSLyxaj.js",
    "name": "chips.esm",
    "src": "node_modules/primevue/chips/chips.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "_CWV7Ebdt.js",
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/colorpicker/colorpicker.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "BggLL9JH.js",
    "name": "colorpicker.esm",
    "src": "node_modules/primevue/colorpicker/colorpicker.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "_D0W5RRF9.js",
      "_DXiRM4vO.js",
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/column/column.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "Bv0u8y6A.js",
    "name": "column.esm",
    "src": "node_modules/primevue/column/column.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/columngroup/columngroup.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "DXIPKt0R.js",
    "name": "columngroup.esm",
    "src": "node_modules/primevue/columngroup/columngroup.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/confirmdialog/confirmdialog.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "DfFtyz4R.js",
    "name": "confirmdialog.esm",
    "src": "node_modules/primevue/confirmdialog/confirmdialog.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js",
      "node_modules/primevue/dialog/dialog.esm.js",
      "_BWk1zTJo.js",
      "_DXiRM4vO.js"
    ]
  },
  "node_modules/primevue/confirmpopup/confirmpopup.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "ClCArSAj.js",
    "name": "confirmpopup.esm",
    "src": "node_modules/primevue/confirmpopup/confirmpopup.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js",
      "_D0W5RRF9.js",
      "_DXiRM4vO.js"
    ]
  },
  "node_modules/primevue/contextmenu/contextmenu.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "D8CDWmzJ.js",
    "name": "contextmenu.esm",
    "src": "node_modules/primevue/contextmenu/contextmenu.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "_DXiRM4vO.js",
      "node_modules/nuxt/dist/app/entry.js",
      "_CYl7PQbN.js"
    ]
  },
  "node_modules/primevue/datatable/datatable.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "RDOhOg8b.js",
    "name": "datatable.esm",
    "src": "node_modules/primevue/datatable/datatable.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js",
      "node_modules/primevue/paginator/paginator.esm.js",
      "node_modules/primevue/virtualscroller/virtualscroller.esm.js",
      "_DSTw9jQC.js",
      "_p_ZPdm--.js",
      "_D24Q6d7X.js",
      "_HJSzoE6Z.js",
      "_BWk1zTJo.js",
      "_D0W5RRF9.js",
      "node_modules/primevue/checkbox/checkbox.esm.js",
      "node_modules/primevue/radiobutton/radiobutton.esm.js",
      "node_modules/primevue/dropdown/dropdown.esm.js",
      "_BOyqhDOU.js",
      "_DXiRM4vO.js",
      "_DXGnkx1y.js",
      "_D9Qh6dT5.js",
      "node_modules/primevue/inputnumber/inputnumber.esm.js",
      "_DNYGth56.js",
      "_BhPlg7EZ.js",
      "node_modules/primevue/inputtext/inputtext.esm.js",
      "_CYl7PQbN.js",
      "_mvKC_REM.js"
    ]
  },
  "node_modules/primevue/dataview/dataview.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "DvtFOPfS.js",
    "name": "dataview.esm",
    "src": "node_modules/primevue/dataview/dataview.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/primevue/paginator/paginator.esm.js",
      "node_modules/nuxt/dist/app/entry.js",
      "_D9Qh6dT5.js",
      "node_modules/primevue/dropdown/dropdown.esm.js",
      "_HJSzoE6Z.js",
      "_DSTw9jQC.js",
      "_mvKC_REM.js",
      "_BWk1zTJo.js",
      "_D0W5RRF9.js",
      "_DXiRM4vO.js",
      "node_modules/primevue/virtualscroller/virtualscroller.esm.js",
      "node_modules/primevue/inputnumber/inputnumber.esm.js",
      "_DNYGth56.js",
      "_BhPlg7EZ.js",
      "node_modules/primevue/inputtext/inputtext.esm.js",
      "_CYl7PQbN.js"
    ]
  },
  "node_modules/primevue/dataviewlayoutoptions/dataviewlayoutoptions.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "CFRrGI9k.js",
    "name": "dataviewlayoutoptions.esm",
    "src": "node_modules/primevue/dataviewlayoutoptions/dataviewlayoutoptions.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "_D24Q6d7X.js",
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/deferredcontent/deferredcontent.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "DQJ0u--w.js",
    "name": "deferredcontent.esm",
    "src": "node_modules/primevue/deferredcontent/deferredcontent.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/dialog/dialog.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "2zUjHWxi.js",
    "name": "dialog.esm",
    "src": "node_modules/primevue/dialog/dialog.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js",
      "_BWk1zTJo.js",
      "_DXiRM4vO.js"
    ]
  },
  "node_modules/primevue/divider/divider.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "nFDVQSXY.js",
    "name": "divider.esm",
    "src": "node_modules/primevue/divider/divider.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/dock/dock.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "BdgLxd6j.js",
    "name": "dock.esm",
    "src": "node_modules/primevue/dock/dock.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/dropdown/dropdown.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "BygkiUTy.js",
    "name": "dropdown.esm",
    "src": "node_modules/primevue/dropdown/dropdown.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js",
      "_HJSzoE6Z.js",
      "_DSTw9jQC.js",
      "_mvKC_REM.js",
      "_BWk1zTJo.js",
      "_D0W5RRF9.js",
      "_DXiRM4vO.js",
      "node_modules/primevue/virtualscroller/virtualscroller.esm.js"
    ]
  },
  "node_modules/primevue/dynamicdialog/dynamicdialog.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "A6vk1Fwm.js",
    "name": "dynamicdialog.esm",
    "src": "node_modules/primevue/dynamicdialog/dynamicdialog.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/primevue/dialog/dialog.esm.js",
      "node_modules/nuxt/dist/app/entry.js",
      "_BWk1zTJo.js",
      "_DXiRM4vO.js"
    ]
  },
  "node_modules/primevue/editor/editor.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "D3v21q33.js",
    "name": "editor.esm",
    "src": "node_modules/primevue/editor/editor.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ],
    "dynamicImports": [
      "node_modules/quill/quill.js"
    ]
  },
  "node_modules/primevue/fieldset/fieldset.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "DLY0_vLL.js",
    "name": "fieldset.esm",
    "src": "node_modules/primevue/fieldset/fieldset.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "_C_uFE9Zw.js",
      "_BOyqhDOU.js",
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/fileupload/fileupload.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "DmrnHgH2.js",
    "name": "fileupload.esm",
    "src": "node_modules/primevue/fileupload/fileupload.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js",
      "_BOyqhDOU.js",
      "_BWk1zTJo.js",
      "node_modules/primevue/message/message.esm.js",
      "node_modules/primevue/progressbar/progressbar.esm.js",
      "_HJSzoE6Z.js",
      "_DiQfV1Th.js",
      "_CWV7Ebdt.js"
    ]
  },
  "node_modules/primevue/floatlabel/floatlabel.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "rirQAwh8.js",
    "name": "floatlabel.esm",
    "src": "node_modules/primevue/floatlabel/floatlabel.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/iconfield/iconfield.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "nXWOnEVk.js",
    "name": "iconfield.esm",
    "src": "node_modules/primevue/iconfield/iconfield.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/image/image.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "AXRWm100.js",
    "name": "image.esm",
    "src": "node_modules/primevue/image/image.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js",
      "_Dsisqutq.js",
      "_BWk1zTJo.js",
      "_DXiRM4vO.js"
    ]
  },
  "node_modules/primevue/inlinemessage/inlinemessage.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "C9rDizZV.js",
    "name": "inlinemessage.esm",
    "src": "node_modules/primevue/inlinemessage/inlinemessage.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "_HJSzoE6Z.js",
      "_DiQfV1Th.js",
      "_CWV7Ebdt.js",
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/inplace/inplace.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "B5_-EYlU.js",
    "name": "inplace.esm",
    "src": "node_modules/primevue/inplace/inplace.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js",
      "_BWk1zTJo.js"
    ]
  },
  "node_modules/primevue/inputgroup/inputgroup.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "E4AWvAmd.js",
    "name": "inputgroup.esm",
    "src": "node_modules/primevue/inputgroup/inputgroup.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/inputgroupaddon/inputgroupaddon.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "CJ_Arr94.js",
    "name": "inputgroupaddon.esm",
    "src": "node_modules/primevue/inputgroupaddon/inputgroupaddon.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/inputicon/inputicon.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "C6rF0lby.js",
    "name": "inputicon.esm",
    "src": "node_modules/primevue/inputicon/inputicon.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/inputmask/inputmask.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "BK-FLImE.js",
    "name": "inputmask.esm",
    "src": "node_modules/primevue/inputmask/inputmask.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/inputnumber/inputnumber.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "C8lvphiM.js",
    "name": "inputnumber.esm",
    "src": "node_modules/primevue/inputnumber/inputnumber.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js",
      "_DNYGth56.js",
      "_BhPlg7EZ.js",
      "node_modules/primevue/inputtext/inputtext.esm.js"
    ]
  },
  "node_modules/primevue/inputotp/inputotp.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "CsGPwicd.js",
    "name": "inputotp.esm",
    "src": "node_modules/primevue/inputotp/inputotp.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/primevue/inputtext/inputtext.esm.js",
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/inputswitch/inputswitch.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "DmKC1yrq.js",
    "name": "inputswitch.esm",
    "src": "node_modules/primevue/inputswitch/inputswitch.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/inputtext/inputtext.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "CiOoj-gI.js",
    "name": "inputtext.esm",
    "src": "node_modules/primevue/inputtext/inputtext.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/knob/knob.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "JxP9rfGs.js",
    "name": "knob.esm",
    "src": "node_modules/primevue/knob/knob.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/listbox/listbox.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "V8M2nf-v.js",
    "name": "listbox.esm",
    "src": "node_modules/primevue/listbox/listbox.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js",
      "_mvKC_REM.js",
      "node_modules/primevue/virtualscroller/virtualscroller.esm.js"
    ]
  },
  "node_modules/primevue/megamenu/megamenu.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "oFkFJg6A.js",
    "name": "megamenu.esm",
    "src": "node_modules/primevue/megamenu/megamenu.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "_D24Q6d7X.js",
      "node_modules/nuxt/dist/app/entry.js",
      "_DNYGth56.js",
      "_CYl7PQbN.js"
    ]
  },
  "node_modules/primevue/menu/menu.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "CPs0tGeQ.js",
    "name": "menu.esm",
    "src": "node_modules/primevue/menu/menu.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "_D0W5RRF9.js",
      "_DXiRM4vO.js",
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/menubar/menubar.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "BvFZ2WVa.js",
    "name": "menubar.esm",
    "src": "node_modules/primevue/menubar/menubar.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "_D24Q6d7X.js",
      "node_modules/nuxt/dist/app/entry.js",
      "_DNYGth56.js",
      "_CYl7PQbN.js"
    ]
  },
  "node_modules/primevue/message/message.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "CPmU7Mla.js",
    "name": "message.esm",
    "src": "node_modules/primevue/message/message.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "_HJSzoE6Z.js",
      "_DiQfV1Th.js",
      "_BWk1zTJo.js",
      "_CWV7Ebdt.js",
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/metergroup/metergroup.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "C_lWgaZv.js",
    "name": "metergroup.esm",
    "src": "node_modules/primevue/metergroup/metergroup.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/multiselect/multiselect.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "Brmpfh4d.js",
    "name": "multiselect.esm",
    "src": "node_modules/primevue/multiselect/multiselect.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js",
      "node_modules/primevue/checkbox/checkbox.esm.js",
      "_HJSzoE6Z.js",
      "_DSTw9jQC.js",
      "_mvKC_REM.js",
      "_BWk1zTJo.js",
      "_CWV7Ebdt.js",
      "_D0W5RRF9.js",
      "_DXiRM4vO.js",
      "node_modules/primevue/virtualscroller/virtualscroller.esm.js"
    ]
  },
  "node_modules/primevue/orderlist/orderlist.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "CwIJTWsG.js",
    "name": "orderlist.esm",
    "src": "node_modules/primevue/orderlist/orderlist.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js",
      "_DfgDGet4.js",
      "_DNYGth56.js",
      "_BhPlg7EZ.js"
    ]
  },
  "node_modules/primevue/organizationchart/organizationchart.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "Bs2b-QcL.js",
    "name": "organizationchart.esm",
    "src": "node_modules/primevue/organizationchart/organizationchart.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js",
      "_DSTw9jQC.js",
      "_BpvOsy_P.js"
    ]
  },
  "node_modules/primevue/overlaypanel/overlaypanel.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "1XZwPFgz.js",
    "name": "overlaypanel.esm",
    "src": "node_modules/primevue/overlaypanel/overlaypanel.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js",
      "_BWk1zTJo.js",
      "_D0W5RRF9.js",
      "_DXiRM4vO.js"
    ]
  },
  "node_modules/primevue/paginator/paginator.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "V1Ra_zrg.js",
    "name": "paginator.esm",
    "src": "node_modules/primevue/paginator/paginator.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js",
      "_D9Qh6dT5.js",
      "node_modules/primevue/dropdown/dropdown.esm.js",
      "node_modules/primevue/inputnumber/inputnumber.esm.js",
      "_CYl7PQbN.js",
      "_HJSzoE6Z.js",
      "_DSTw9jQC.js",
      "_mvKC_REM.js",
      "_BWk1zTJo.js",
      "_D0W5RRF9.js",
      "_DXiRM4vO.js",
      "node_modules/primevue/virtualscroller/virtualscroller.esm.js",
      "_DNYGth56.js",
      "_BhPlg7EZ.js",
      "node_modules/primevue/inputtext/inputtext.esm.js"
    ]
  },
  "node_modules/primevue/panel/panel.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "DylaeGIr.js",
    "name": "panel.esm",
    "src": "node_modules/primevue/panel/panel.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "_C_uFE9Zw.js",
      "_BOyqhDOU.js",
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/panelmenu/panelmenu.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "BFiVo-vU.js",
    "name": "panelmenu.esm",
    "src": "node_modules/primevue/panelmenu/panelmenu.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "_DSTw9jQC.js",
      "_p_ZPdm--.js",
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/password/password.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "DRJtV_en.js",
    "name": "password.esm",
    "src": "node_modules/primevue/password/password.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "_Dsisqutq.js",
      "node_modules/nuxt/dist/app/entry.js",
      "node_modules/primevue/inputtext/inputtext.esm.js",
      "_D0W5RRF9.js",
      "_DXiRM4vO.js"
    ]
  },
  "node_modules/primevue/picklist/picklist.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "BOQFhxCF.js",
    "name": "picklist.esm",
    "src": "node_modules/primevue/picklist/picklist.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js",
      "_DfgDGet4.js",
      "_D9Qh6dT5.js",
      "_DNYGth56.js",
      "_CYl7PQbN.js",
      "_BhPlg7EZ.js"
    ]
  },
  "node_modules/primevue/progressbar/progressbar.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "DXelVzlp.js",
    "name": "progressbar.esm",
    "src": "node_modules/primevue/progressbar/progressbar.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/progressspinner/progressspinner.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "D3YHcB8H.js",
    "name": "progressspinner.esm",
    "src": "node_modules/primevue/progressspinner/progressspinner.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/radiobutton/radiobutton.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "Cd5qp1cw.js",
    "name": "radiobutton.esm",
    "src": "node_modules/primevue/radiobutton/radiobutton.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/rating/rating.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "BiaXoEjm.js",
    "name": "rating.esm",
    "src": "node_modules/primevue/rating/rating.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/resources/themes/aura-light-green/fonts/Inter-italic.var.woff2": {
    "resourceType": "font",
    "mimeType": "font/woff2",
    "file": "Inter-italic.var.DhD-tpjY.woff2",
    "src": "node_modules/primevue/resources/themes/aura-light-green/fonts/Inter-italic.var.woff2"
  },
  "node_modules/primevue/resources/themes/aura-light-green/fonts/Inter-roman.var.woff2": {
    "resourceType": "font",
    "mimeType": "font/woff2",
    "file": "Inter-roman.var.C-r5W2Hj.woff2",
    "src": "node_modules/primevue/resources/themes/aura-light-green/fonts/Inter-roman.var.woff2"
  },
  "node_modules/primevue/row/row.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "BP5PD4-5.js",
    "name": "row.esm",
    "src": "node_modules/primevue/row/row.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/scrollpanel/scrollpanel.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "Dhx7MXHn.js",
    "name": "scrollpanel.esm",
    "src": "node_modules/primevue/scrollpanel/scrollpanel.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/scrolltop/scrolltop.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "Dmu4boFe.js",
    "name": "scrolltop.esm",
    "src": "node_modules/primevue/scrolltop/scrolltop.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "_BpvOsy_P.js",
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/selectbutton/selectbutton.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "BszuiBKn.js",
    "name": "selectbutton.esm",
    "src": "node_modules/primevue/selectbutton/selectbutton.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/sidebar/sidebar.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "_yx_2Saq.js",
    "name": "sidebar.esm",
    "src": "node_modules/primevue/sidebar/sidebar.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js",
      "_BWk1zTJo.js",
      "_DXiRM4vO.js"
    ]
  },
  "node_modules/primevue/skeleton/skeleton.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "dJYVomQx.js",
    "name": "skeleton.esm",
    "src": "node_modules/primevue/skeleton/skeleton.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/slider/slider.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "DbHBntRQ.js",
    "name": "slider.esm",
    "src": "node_modules/primevue/slider/slider.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/speeddial/speeddial.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "U-2VAUup.js",
    "name": "speeddial.esm",
    "src": "node_modules/primevue/speeddial/speeddial.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js",
      "_BOyqhDOU.js"
    ]
  },
  "node_modules/primevue/splitbutton/splitbutton.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "P7jcCesr.js",
    "name": "splitbutton.esm",
    "src": "node_modules/primevue/splitbutton/splitbutton.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js",
      "_DSTw9jQC.js",
      "node_modules/primevue/tieredmenu/tieredmenu.esm.js",
      "_D0W5RRF9.js",
      "_DXiRM4vO.js",
      "_CYl7PQbN.js"
    ]
  },
  "node_modules/primevue/splitter/splitter.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "CBoeVhow.js",
    "name": "splitter.esm",
    "src": "node_modules/primevue/splitter/splitter.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/splitterpanel/splitterpanel.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "BLK5V-eX.js",
    "name": "splitterpanel.esm",
    "src": "node_modules/primevue/splitterpanel/splitterpanel.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/stepper/stepper.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "BdzN4Lfc.js",
    "name": "stepper.esm",
    "src": "node_modules/primevue/stepper/stepper.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/stepperpanel/stepperpanel.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "KNXNSvto.js",
    "name": "stepperpanel.esm",
    "src": "node_modules/primevue/stepperpanel/stepperpanel.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/steps/steps.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "OB4wJUQQ.js",
    "name": "steps.esm",
    "src": "node_modules/primevue/steps/steps.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/tabmenu/tabmenu.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "nNt4dCSJ.js",
    "name": "tabmenu.esm",
    "src": "node_modules/primevue/tabmenu/tabmenu.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/tabpanel/tabpanel.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "W7xGSOOb.js",
    "name": "tabpanel.esm",
    "src": "node_modules/primevue/tabpanel/tabpanel.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/tabview/tabview.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "Bjw61lGl.js",
    "name": "tabview.esm",
    "src": "node_modules/primevue/tabview/tabview.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "_pb5u_aX1.js",
      "_p_ZPdm--.js",
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/tag/tag.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "DVGofZRF.js",
    "name": "tag.esm",
    "src": "node_modules/primevue/tag/tag.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/terminal/terminal.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "BU7qcRHe.js",
    "name": "terminal.esm",
    "src": "node_modules/primevue/terminal/terminal.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/textarea/textarea.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "H7M6t5l7.js",
    "name": "textarea.esm",
    "src": "node_modules/primevue/textarea/textarea.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/tieredmenu/tieredmenu.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "Bha5kRZV.js",
    "name": "tieredmenu.esm",
    "src": "node_modules/primevue/tieredmenu/tieredmenu.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "_D0W5RRF9.js",
      "_DXiRM4vO.js",
      "node_modules/nuxt/dist/app/entry.js",
      "_CYl7PQbN.js"
    ]
  },
  "node_modules/primevue/timeline/timeline.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "zytk0JQ3.js",
    "name": "timeline.esm",
    "src": "node_modules/primevue/timeline/timeline.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/toast/toast.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "DG-rzs9J.js",
    "name": "toast.esm",
    "src": "node_modules/primevue/toast/toast.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "_DXiRM4vO.js",
      "node_modules/nuxt/dist/app/entry.js",
      "_HJSzoE6Z.js",
      "_DiQfV1Th.js",
      "_BWk1zTJo.js",
      "_CWV7Ebdt.js"
    ]
  },
  "node_modules/primevue/togglebutton/togglebutton.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "BGnmPWcF.js",
    "name": "togglebutton.esm",
    "src": "node_modules/primevue/togglebutton/togglebutton.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/toolbar/toolbar.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "DyeoBt-m.js",
    "name": "toolbar.esm",
    "src": "node_modules/primevue/toolbar/toolbar.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/tree/tree.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "DM5s1SFV.js",
    "name": "tree.esm",
    "src": "node_modules/primevue/tree/tree.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "_mvKC_REM.js",
      "node_modules/nuxt/dist/app/entry.js",
      "node_modules/primevue/checkbox/checkbox.esm.js",
      "_HJSzoE6Z.js",
      "_DSTw9jQC.js",
      "_p_ZPdm--.js",
      "_C_uFE9Zw.js"
    ]
  },
  "node_modules/primevue/treeselect/treeselect.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "I5WuTyrm.js",
    "name": "treeselect.esm",
    "src": "node_modules/primevue/treeselect/treeselect.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "_DSTw9jQC.js",
      "_D0W5RRF9.js",
      "_DXiRM4vO.js",
      "node_modules/nuxt/dist/app/entry.js",
      "node_modules/primevue/tree/tree.esm.js",
      "_mvKC_REM.js",
      "node_modules/primevue/checkbox/checkbox.esm.js",
      "_HJSzoE6Z.js",
      "_p_ZPdm--.js",
      "_C_uFE9Zw.js"
    ]
  },
  "node_modules/primevue/treetable/treetable.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "BaSYnUVi.js",
    "name": "treetable.esm",
    "src": "node_modules/primevue/treetable/treetable.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js",
      "node_modules/primevue/paginator/paginator.esm.js",
      "_DXGnkx1y.js",
      "node_modules/primevue/checkbox/checkbox.esm.js",
      "_HJSzoE6Z.js",
      "_DSTw9jQC.js",
      "_p_ZPdm--.js",
      "_C_uFE9Zw.js",
      "_D9Qh6dT5.js",
      "node_modules/primevue/dropdown/dropdown.esm.js",
      "_mvKC_REM.js",
      "_BWk1zTJo.js",
      "_D0W5RRF9.js",
      "_DXiRM4vO.js",
      "node_modules/primevue/virtualscroller/virtualscroller.esm.js",
      "node_modules/primevue/inputnumber/inputnumber.esm.js",
      "_DNYGth56.js",
      "_BhPlg7EZ.js",
      "node_modules/primevue/inputtext/inputtext.esm.js",
      "_CYl7PQbN.js"
    ]
  },
  "node_modules/primevue/tristatecheckbox/tristatecheckbox.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "Cp3VMrka.js",
    "name": "tristatecheckbox.esm",
    "src": "node_modules/primevue/tristatecheckbox/tristatecheckbox.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "_HJSzoE6Z.js",
      "_BWk1zTJo.js",
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/primevue/virtualscroller/virtualscroller.esm.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "CgDC0Swl.js",
    "name": "virtualscroller.esm",
    "src": "node_modules/primevue/virtualscroller/virtualscroller.esm.js",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/quill/quill.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "BqYzQad1.js",
    "name": "quill",
    "src": "node_modules/quill/quill.js",
    "isDynamicEntry": true
  },
  "pages/[id].vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "DBbGlmmf.js",
    "name": "_id_",
    "src": "pages/[id].vue",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "pages/about.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "CE4ZUXIf.js",
    "name": "about",
    "src": "pages/about.vue",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "pages/about/about-1.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "DWmkkz9J.js",
    "name": "about-1",
    "src": "pages/about/about-1.vue",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "pages/about/index.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "Bjryr2YT.js",
    "name": "index",
    "src": "pages/about/index.vue",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "pages/assets-sample.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "BUCrezDk.js",
    "name": "assets-sample",
    "src": "pages/assets-sample.vue",
    "isDynamicEntry": true,
    "imports": [
      "_BwBznSD0.js",
      "node_modules/nuxt/dist/app/entry.js"
    ],
    "css": [],
    "assets": [
      "nuxt-asset.Do-rDTHT.png"
    ]
  },
  "assets-sample.goKVs1AX.css": {
    "file": "assets-sample.goKVs1AX.css",
    "resourceType": "style",
    "prefetch": true,
    "preload": true
  },
  "nuxt-asset.Do-rDTHT.png": {
    "file": "nuxt-asset.Do-rDTHT.png",
    "resourceType": "image",
    "prefetch": true,
    "mimeType": "image/png"
  },
  "pages/categories.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "PSSehNNl.js",
    "name": "categories",
    "src": "pages/categories.vue",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "pages/categories/[category].vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "NDYvt5o9.js",
    "name": "_category_",
    "src": "pages/categories/[category].vue",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "pages/categories/[category]/[product].vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "Ck1BcnFs.js",
    "name": "_product_",
    "src": "pages/categories/[category]/[product].vue",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "pages/categories/[category]/index.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "BEgo-Soa.js",
    "name": "index",
    "src": "pages/categories/[category]/index.vue",
    "isDynamicEntry": true,
    "imports": [
      "_qMGSVOoo.js",
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "pages/categories/index.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "emR0KO_f.js",
    "name": "index",
    "src": "pages/categories/index.vue",
    "isDynamicEntry": true,
    "imports": [
      "_qMGSVOoo.js",
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "pages/fetching/base.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "CuajwuqK.js",
    "name": "base",
    "src": "pages/fetching/base.vue",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js",
      "_CygsLJbz.js"
    ]
  },
  "pages/fetching/fetching-deduplication.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "Do2hrB9q.js",
    "name": "fetching-deduplication",
    "src": "pages/fetching/fetching-deduplication.vue",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js",
      "_CygsLJbz.js"
    ]
  },
  "pages/fetching/fetching.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "DQ3a_6jC.js",
    "name": "fetching",
    "src": "pages/fetching/fetching.vue",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js",
      "_CygsLJbz.js"
    ]
  },
  "pages/fetching/post.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "DdxsJDyz.js",
    "name": "post",
    "src": "pages/fetching/post.vue",
    "isDynamicEntry": true,
    "imports": [
      "_CygsLJbz.js",
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "pages/fetching/put.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "BhrA32VM.js",
    "name": "put",
    "src": "pages/fetching/put.vue",
    "isDynamicEntry": true,
    "imports": [
      "_CygsLJbz.js",
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "pages/fetching/watch.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "CXWDDXvz.js",
    "name": "watch",
    "src": "pages/fetching/watch.vue",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js",
      "_CygsLJbz.js"
    ]
  },
  "pages/index.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "byjCRqL3.js",
    "name": "index",
    "src": "pages/index.vue",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "pages/lazy-loading/index.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "D6zC-Zd-.js",
    "name": "index",
    "src": "pages/lazy-loading/index.vue",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ],
    "dynamicImports": [
      "components/Welcome.vue"
    ]
  },
  "pages/product/[id].vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "Bvw45mn3.js",
    "name": "_id_",
    "src": "pages/product/[id].vue",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js",
      "_CygsLJbz.js"
    ]
  },
  "pages/services.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "CeT_V-ex.js",
    "name": "services",
    "src": "pages/services.vue",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "pages/states/compos-state.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "DheluBmt.js",
    "name": "compos-state",
    "src": "pages/states/compos-state.vue",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js",
      "_qMGSVOoo.js",
      "_HOLQHh74.js",
      "_D6mLI-j_.js"
    ]
  },
  "pages/states/shallow.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "DEEwvJTC.js",
    "name": "shallow",
    "src": "pages/states/shallow.vue",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js",
      "_qMGSVOoo.js",
      "_D6mLI-j_.js"
    ]
  },
  "pages/states/shared-state.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "BDlfG2BT.js",
    "name": "shared-state",
    "src": "pages/states/shared-state.vue",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js",
      "_qMGSVOoo.js",
      "_D6mLI-j_.js"
    ]
  },
  "pages/states/state1.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "BcVVPJxj.js",
    "name": "state1",
    "src": "pages/states/state1.vue",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js",
      "_qMGSVOoo.js",
      "_D6mLI-j_.js",
      "_HOLQHh74.js"
    ]
  },
  "pages/states/state2.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "B-zqDn32.js",
    "name": "state2",
    "src": "pages/states/state2.vue",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js",
      "_qMGSVOoo.js",
      "_D6mLI-j_.js"
    ]
  },
  "pages/user-[id].vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "hnEbe4mi.js",
    "name": "user-_id_",
    "src": "pages/user-[id].vue",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "pages/users/[id].vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "DgyF8miA.js",
    "name": "_id_",
    "src": "pages/users/[id].vue",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "pages/users/[name]/[name].vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "DQcrnGf_.js",
    "name": "_name_",
    "src": "pages/users/[name]/[name].vue",
    "isDynamicEntry": true,
    "imports": [
      "_BwBznSD0.js",
      "node_modules/nuxt/dist/app/entry.js"
    ]
  }
};

export { client_manifest as default };
//# sourceMappingURL=client.manifest.mjs.map
