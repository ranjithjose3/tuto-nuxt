export default defineNuxtPlugin(() => {
    addRouteMiddleware(( to, from) => {
        if(to.path === '/database'){
            return abortNavigation('Forbidden')
        }
    })

    addRouteMiddleware('denyNoor',(to, from) => {
        const name = to.params.name
        if(name === 'noor') return abortNavigation('The name noor is not allowed') 
    })

    addRouteMiddleware(() => {
        console.log('Golbal Middleware')
    }, {global: true} )


    addRouteMiddleware((to) => {
        if (to.fullPath.startsWith('/name')) {
            to.meta.layout = 'admin'
          } else {
            to.meta.layout = 'default'
          }
    }, {global: true} )




})