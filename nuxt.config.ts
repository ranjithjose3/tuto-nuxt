// nuxt.config.ts
import { defineNuxtConfig } from 'nuxt/config'

export default defineNuxtConfig({
  target: 'static',
  ssr: true,
  generate: {
    fallback: true,
    dir: 'dist'
  },
  nitro: {
    preset: 'netlify'
  },
  buildModules: [
    '@nuxtjs/devtools',
  ],
  devtools: { enabled: true },
  modules: [
    'nuxt-primevue',
    '@vee-validate/nuxt',
  ],
  primevue: {
    cssLayerOrder: 'reset,primevue',
    components: {
      prefix: 'Prime',
      include: '*',
      exclude: ['Galleria', 'Carousel']
    },
    directives: {
      include: '*',
    }
  },
  veeValidate: {
    autoImports: true,
    componentNames: {
      Form: 'VeeForm',
      Field: 'VeeField',
      FieldArray: 'VeeFieldArray',
      ErrorMessage: 'VeeErrorMessage',
    },
  },
  // Font Awesome Configuration (Global Registration)
  app: {
    head: {
      link: [
        // ... (your other link tags)
      ],
      style: [
        // ... (your other style tags)
      ],
      script: [
        // ... (your other script tags)
      ]
    },
    pageTransition: {
      name: 'page',
      mode: 'out-in'
    }
  },
  css: [
    'primeflex/primeflex.css',
    'primevue/resources/themes/aura-light-green/theme.css',
    'animate.css',
    'primeicons/primeicons.css',
    '~/assets/styles/custom-style.css',
  ],
  plugins: [
    '~/plugins/fontawesome.js' // Register the Font Awesome plugin
  ]
});
